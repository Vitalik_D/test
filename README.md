Run the project

* Open terminal 1 in directory server
  * Run command ```npm i```
  * Run command ```node index.js```
* Open terminal 2 in directory client
    * Run command ```npm i```
    * Run command ```npm start```

Further instructions can be found at [Home Page](http://localhost:3000)