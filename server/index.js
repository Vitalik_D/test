const cors = require('cors')
const puppeteer = require('puppeteer')
const express = require('express')

const app = express()

app.use(cors())

app.get('/genre-list', async (req, res) => {
    const browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null,
    })
    const page = await browser.newPage()
    let genreList
    const findGenres = async () => {
        try {
            await page.goto(`https://www.goodreads.com/genres`)
            genreList = await page.$$eval('div.bigBoxContent a.gr-hyperlink', (genres) => genres.map(genre => genre.textContent))
        } catch (error) {
            res.send(error)
        }
        await browser.close()
    }
    await findGenres()
    await res.send(genreList)
})

app.get('/find-buy-book', async (req, res) => {
    const browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null,
    })

    const page = await browser.newPage()
    const buyBook = async () => {
        await page.goto(`https://www.goodreads.com/genres/${req.query.genre}`)
        const listLinkBooks = await page.$$eval('div.coverWrapper a', (links) => links.map(link => link.href))
        if (!!listLinkBooks.length) {
            const randomLink = Math.floor(Math.random() * listLinkBooks.length)
            await page.goto(listLinkBooks[randomLink])
            const bookTitle = await page.$eval('h1#bookTitle', (title) => title.textContent)
            await page.goto(`https://www.amazon.com/s?k=${bookTitle}`)
            await page.evaluate(() => {
                document.getElementsByClassName("s-image")[0].click()
            })
            await page.waitForNavigation()
            const isBuyNowAvailable = await page.evaluate(() => !!document.getElementById("buy-now-button"))
            const buyOneClick = await page.evaluate(() => !!document.getElementById("one-click-button"))
            const notAvailableForBuy = !isBuyNowAvailable && !buyOneClick
            if (isBuyNowAvailable) {
                await page.evaluate(() => {
                    document.getElementById("buy-now-button").click()
                })
            }
            if (buyOneClick) {
                await page.evaluate(() => {
                    document.getElementById("one-click-button").click()
                })
            }
            if (notAvailableForBuy) {
                await buyBook()
            }
            res.send(res.status(200).send({data: 'Good job'}))
        } else {
            res.status(402).send(new Error('There is no such genre'))
            await browser.close()
        }
    }
    await buyBook()
})

app.listen(4001, () => {
    console.log('server run port 4001')
})