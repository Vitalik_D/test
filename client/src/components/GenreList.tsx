import React, {useState} from "react"
import axios from "axios"
import Button from "@mui/material/Button"

interface GenreListProp {
  findBuyBook: (element: string) => void
}

const GenreList = ({findBuyBook}: GenreListProp) => {
  const [genreList, setGenreList] = useState([])
  const [listError, setListError] = useState(false)

  const fetchGenreBookList = () => {
    listError && setListError(false)
    axios.get('http://localhost:4001/genre-list')
      .then(({data}): void => setGenreList(data))
      .catch(() => setListError(true))
  }

  return (
    <div className='list-block'>
      {genreList.length === 0
        ? <Button variant="contained" onClick={fetchGenreBookList}>Get genre list</Button>
        : <ul className='list-genre'>
          {genreList.map(element => (
            <li className='list-element' onClick={() => findBuyBook(element)} key={element}>
              {element}
            </li>
          ))}
        </ul>}
      {listError && <div className='error'>An error occurred, please try again later</div>}
    </div>
  )
}

export default GenreList