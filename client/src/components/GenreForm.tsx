import React, {useState} from "react"
import TextField from "@mui/material/TextField"
import Button from "@mui/material/Button"

interface GenreFormProp {
  findBuyBook: (inputValue: string) => void,
  error: boolean,
  emptyGenre: boolean
}

const GenreForm = ({findBuyBook, error, emptyGenre}: GenreFormProp) => {
  const [inputValue, setInputValue] = useState('')
  const disableFormBtn = !inputValue

  const handleChange = (value: any) => {
    setInputValue(value)
  }
  return (
    <>
      <div className="form-block">
        <TextField
          id="outlined-required"
          label="Book genre"
          value={inputValue}
          onChange={(event) => handleChange(event.target.value)}
        />
        <Button disabled={disableFormBtn} variant="contained" onClick={() => findBuyBook(inputValue)}>
          <p>Send</p>
        </Button>
      </div>
      {error && <div className='error'><p>An error occurred, please try again later</p></div>}
      {emptyGenre && <div className='error'><p>There is no such genre</p></div>}
    </>

  )
}

export default GenreForm