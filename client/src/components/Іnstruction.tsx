const Instruction = () => (
  <div className="instruction-block">
    <p>Hi, this app will help you in choosing a book.</p>
    <p> I'll tell you how to choose a book.</p>
    <p>Please note before you start working with the application, log in to <a href="https://www.amazon.com/">Amazon.</a></p>
    <p>
      Let's return to the choice of the book.
      To get a list, click "Get genre list".
      Choose the genre you like.
      If none of the genres came up, try entering it in the field and enter send button.
    </p>
  </div>
)

export default Instruction