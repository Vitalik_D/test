import React, {useState} from 'react'
import axios from "axios"
import Instruction from "./components/Іnstruction"
import GenreList from "./components/GenreList"
import GenreForm from "./components/GenreForm"

const App = () => {
  const [error, setError] = useState(false)
  const [emptyGenre, setEmptyGenre] = useState(false)

  const findBuyBook = (genre: string) => {
    emptyGenre && setEmptyGenre(false)
    error && setError(false)
    axios.get(`http://localhost:4001/find-buy-book?genre=${genre}`)
      .then(() => {
      })
      .catch((error) => {
        error.response.status === 402
          ? setEmptyGenre(true)
          : setError(true)
      })
  }
  return (
    <div className="app">
      <Instruction/>
      <GenreList findBuyBook={findBuyBook}/>
      <GenreForm findBuyBook={findBuyBook} error={error} emptyGenre={emptyGenre}/>
    </div>
  )
}

export default App
